console.log("Hello World!");
let variavel = -5;
console.log("variavel " + variavel + " é do tipo " + typeof(variavel));

let numero1 = 5;
let numero2 = 10;

console.log(""+numero1+""+""numero2);

console.log(5*2);

let idade = 40;

if(idade == undefined){
    console.log("Idade não foi criada")
}else{
    console.log("Sua idade é: " + idade);
}catch(error){
    console.log("Erro na variavel idade!");

}

function mostrarIdade(){
    let idade = 42;
    if(idade < 42){
        console.log("Jovem");
    }else{
        console.log("Não muito jovem");
    }
}

mostrarIdade();


function mostrarIdade2(vlIdade){
    if(vlIdade < 42){
        console.log(vlIdade + " - Jovem");

    }else{
        console.log(vlIdade + " - Não muito Jovem");
    }
}

mostrarIdade2(42);


const mostrarIdade3 = (vlIdade) => {
    if(vlIdade < 42){
        return vlIdade + " - Jovem - versão 3";

    }else{
       return vlIdade + " - Não muito Jovem - versão 3";
    }
}


console.log(mostrarIdade3(42));

